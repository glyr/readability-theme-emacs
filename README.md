Readability
===========
Light color theme for Emacs. Ideal for usage with [redshift](http://jonls.dk/redshift/) for eye strain-free experience. 


Installation
------------
	
Copy `readability-theme.el`  in your `.emacs.d` and add the following line in your configuration file.

```lisp
(load-theme 'Readability t)
```

Make sure that you `export TERM="xterm-256color` to be able to have the theme in emacs -nw too.

Screenshot
----------

![alt text](readability-emacs.png)
